import chai from "chai";

import { emit } from "../lib/emit.js";
import { MtbonValue } from "../lib/types.js";
import { getEmitTestItems } from "./data.js";

describe("emit", () => {
  it("does not support characters with a code point greater than U+FF", () => {
    chai.assert.doesNotThrow(() => emit(String.fromCodePoint(0xff)));
    chai.assert.throws(() => emit(String.fromCodePoint(0x100)));
  });

  describe("test data", () => {
    for (const {js, mtbon: expected} of getEmitTestItems()) {
      it(js, () => {
        // eslint-disable-next-line no-eval
        const input: MtbonValue = eval(js);
        const actual: string = emit(input);
        chai.assert.include(expected, actual);
      });
    }
  });
});
