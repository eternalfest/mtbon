import path from "path";
import { fileURLToPath } from "url";

const dirname: string = path.dirname(fileURLToPath(import.meta.url));

export default {dirname};
