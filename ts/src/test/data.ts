import fs from "fs";
import path from "path";

import meta from "./meta.js";

const TEST_DIR: string = path.resolve(meta.dirname, "../../test-resources");
const BIDIRECTIONAL_PATH: string = path.resolve(TEST_DIR, "bidirectional.json");
const EMIT_ONLY_PATH: string = path.resolve(TEST_DIR, "emit-only.json");
const PARSE_ONLY_PATH: string = path.resolve(TEST_DIR, "parse-only.json");

export interface TestItem {
  js: string;
  mtbon: string[];
}

export function getEmitTestItems(): TestItem[] {
  return [...getBidirectionalTestItems(), ...getEmitOnlyTestItems()];
}

export function getParseTestItems(): TestItem[] {
  return [...getBidirectionalTestItems(), ...getParseOnlyTestItems()];
}

function getBidirectionalTestItems(): TestItem[] {
  const bidirectional: TestItem[] = readTestItemsSync(BIDIRECTIONAL_PATH);
  const large: ReadonlyArray<string> = ["lvl10", "torture"];
  for (const item of large) {
    const resolvedJson: string = path.resolve(TEST_DIR, `${item}.json`);
    const resolvedMtbon: string = path.resolve(TEST_DIR, `${item}.mtbon`);
    const json: any = readJsonSync(resolvedJson);
    const mtbon: string[] = [readMtbonSync(resolvedMtbon)];
    const js: string = `(${JSON.stringify(json)})`;
    bidirectional.push({js, mtbon});
  }
  return bidirectional;
}

function getEmitOnlyTestItems(): TestItem[] {
  return readTestItemsSync(EMIT_ONLY_PATH);
}

function getParseOnlyTestItems(): TestItem[] {
  return readTestItemsSync(PARSE_ONLY_PATH);
}

function readTestItemsSync(p: string): TestItem[] {
  const raw: any[] = readJsonSync(p);
  for(const item of raw) {
    if (typeof item.mtbon === "string") {
      item.mtbon = [item.mtbon];
    }
  }
  return raw;
}

function readJsonSync(p: string): any {
  const buffer: Buffer = fs.readFileSync(p);
  return JSON.parse(buffer.toString("utf-8"));
}

function readMtbonSync(p: string): string {
  const buffer: Buffer = fs.readFileSync(p);
  return buffer.toString("utf-8").trim();
}
