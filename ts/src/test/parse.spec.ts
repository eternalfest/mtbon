import chai from "chai";

import { parse } from "../lib/parse.js";
import { MtbonValue } from "../lib/types.js";
import { getParseTestItems } from "./data.js";

describe("parse", () => {
  describe("test data", () => {
    for (const {js, mtbon: inputs} of getParseTestItems()) {
      // eslint-disable-next-line no-eval
      const expected: MtbonValue = eval(js);
      for (const input of inputs) {
        it(input, () => {
          const actual: MtbonValue = parse(input);
          chai.assert.deepEqual(actual, expected);
        });
      }
    }
  });
});
