import { UintSize } from "semantic-types";

export class KeyRegistry {
  private keyToId: Map<string, UintSize>;
  private idToKey: Map<UintSize, string>;
  private idBits: UintSize;

  constructor() {
    this.keyToId = new Map();
    this.idToKey = new Map();
    this.idBits = 0;
  }

  getId(key: string): UintSize | undefined {
    return this.keyToId.get(key);
  }

  getKey(id: UintSize): string | undefined {
    return this.idToKey.get(id);
  }

  getIdBits(): UintSize {
    return this.idBits;
  }

  add(key: string): void {
    const id: UintSize = this.keyToId.size;
    this.keyToId.set(key, id);
    this.idToKey.set(id, key);
    if ((id + 1) === 2 ** this.idBits) {
      this.idBits++;
    }
  }
}
