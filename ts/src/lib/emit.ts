import { isMt64Char, mt64CharToValue, Mt64WritableStream } from "@eternalfest/mt64";
import { Incident } from "incident";
import { Float64, Sint32, Uint16, UintSize } from "semantic-types";

import { KeyRegistry } from "./key-registry.js";
import { MtbonValue } from "./types.js";

export function emit(value: MtbonValue): string {
  const registry: KeyRegistry = new KeyRegistry();
  const stream: Mt64WritableStream = new Mt64WritableStream();
  writeAny(stream, registry, value);
  return stream.getBytes().toString("utf-8");
}

function writeSint32(stream: Mt64WritableStream, value: Sint32): void {
  if (value < 0) {
    stream.writeUint32Bits(3, 0b111);
    value = -value;
  }
  if (value < 2 ** 2) {
    stream.writeUint32Bits(2, 0b00);
    stream.writeUint32Bits(2, value);
  } else if (value < 2 ** 4) {
    stream.writeUint32Bits(2, 0b01);
    stream.writeUint32Bits(4, value);
  } else if (value < 2 ** 6) {
    stream.writeUint32Bits(2, 0b10);
    stream.writeUint32Bits(6, value);
  } else if (value < 2 ** 16) {
    stream.writeUint32Bits(4, 0b1100);
    stream.writeUint32Bits(16, value);
  } else {
    stream.writeUint32Bits(4, 0b1101);
    stream.writeUint32Bits(16, value & 0xffff);
    stream.writeUint32Bits(16, (value >>> 16) & 0xffff);
  }
}

function writeFloat64(stream: Mt64WritableStream, value: Float64): void {
  if (isNaN(value)) {
    stream.writeUint32Bits(2, 0b10);
    return;
  } else if (value === +Infinity) {
    stream.writeUint32Bits(3, 0b110);
    return;
  } else if (value === -Infinity) {
    stream.writeUint32Bits(3, 0b111);
    return;
  }
  stream.writeUint32Bits(1, 0b0);

  const str: string = value.toString(10);
  if (str.length >= 2 ** 5) {
    throw new Incident("Float64LengthOverflow", {value, str});
  }
  stream.writeUint32Bits(5, str.length);
  for (let i: number = 0; i < str.length; i++) {
    const code: Uint16 = str.charCodeAt(i);
    switch (code) {
      case chars.DOT:
        stream.writeUint32Bits(4, 10);
        break;
      case chars.PLUS:
        stream.writeUint32Bits(4, 11);
        break;
      case chars.MINUS:
        stream.writeUint32Bits(4, 12);
        break;
      case chars.LOWER_E:
        stream.writeUint32Bits(4, 13);
        break;
      default:
        if (chars.DIGIT_ZERO <= code && code <= chars.DIGIT_NINE) {
          stream.writeUint32Bits(4, code - chars.DIGIT_ZERO);
        } else {
          throw new Incident("UnexpectedFloat64Char", {value, str});
        }
        break;
    }
  }
}

function writeList(stream: Mt64WritableStream, keyRegistry: KeyRegistry, value: ReadonlyArray<MtbonValue>): void {
  let nullSequenceLength: UintSize = 0;
  for (const item of value) {
    if (item === null) {
      nullSequenceLength++;
      continue;
    }
    if (nullSequenceLength > 0) {
      stream.writeUint32Bits(2, 0b10);
      writeSint32(stream, nullSequenceLength);
      nullSequenceLength = 0;
    }
    stream.writeUint32Bits(1, 0b0);
    writeAny(stream, keyRegistry, item);
  }
  if (nullSequenceLength > 0) {
    stream.writeUint32Bits(2, 0b10);
    writeSint32(stream, nullSequenceLength);
  }
  stream.writeUint32Bits(2, 0b11);
}

function writeLatin1String(stream: Mt64WritableStream, value: string): void {
  // 6 bits: MT64
  // 7 bits: ASCII
  // 8 bits: latin1
  let charBits: UintSize = 6;
  for (let i: number = 0; i < value.length; i++) {
    const code: Uint16 = value.charCodeAt(i);
    if (isMt64Char(value[i])) {
      continue;
    } else if (code < 2 ** 7) { // isAscii
      charBits = Math.max(charBits, 7);
    } else if (code < 2 ** 8) { // isLatin1
      charBits = Math.max(charBits, 8);
    } else {
      throw new Incident("InvalidChar", {value, index: i});
    }
  }

  writeSint32(stream, value.length);
  switch (charBits) {
    case 6:
      stream.writeBoolBits(false);
      for (const char of value) {
        // charBits === 6 ensures that mt64ToValue returns a number
        stream.writeUint32Bits(6, mt64CharToValue(char)!);
      }
      break;
    case 7:
    case 8:
      stream.writeBoolBits(true);
      stream.writeBoolBits(charBits === 8);
      for (let i: number = 0; i < value.length; i++) {
        // charBits ensures that there's no overflow
        stream.writeUint32Bits(charBits, value.charCodeAt(i));
      }
      break;
    default:
      throw new Incident("UnexpectedCharBits", {charBits});
  }
}

function writeDocument(stream: Mt64WritableStream, keyRegistry: KeyRegistry, value: Record<string, MtbonValue>) {
  for (const key of Object.keys(value)) {
    const id: UintSize | undefined = keyRegistry.getId(key);
    if (id !== undefined) {
      stream.writeUint32Bits(1, 0b0);
      stream.writeUint32Bits(keyRegistry.getIdBits(), id);
    } else {
      keyRegistry.add(key);
      stream.writeUint32Bits(2, 0b10);
      writeLatin1String(stream, key);
    }
    writeAny(stream, keyRegistry, value[key]);
  }
  stream.writeUint32Bits(2, 0b11);
}

function writeAny(stream: Mt64WritableStream, keyRegistry: KeyRegistry, value: MtbonValue): void {
  switch (typeof value) {
    case "string":
      stream.writeUint32Bits(4, 0b1110);
      writeLatin1String(stream, value);
      break;
    case "number":
      // Note that `-0x80000000` cannot be written as an Sint32;
      // trying to decode its encoding would give `0x80000000`.
      if ((value | 0) === value && value !== -0x80000000) {
        stream.writeUint32Bits(2, 0b00);
        writeSint32(stream, value);
      } else {
        stream.writeUint32Bits(2, 0b01);
        writeFloat64(stream, value);
      }
      break;
    case "boolean":
      stream.writeUint32Bits(3, 0b110);
      stream.writeBoolBits(value);
      break;
    case "object":
      if (value === null) {
        stream.writeUint32Bits(4, 0b1111);
      } else if (Array.isArray(value)) {
        stream.writeUint32Bits(3, 0b100);
        writeList(stream, keyRegistry, value);
      } else {
        stream.writeUint32Bits(3, 0b101);
        writeDocument(stream, keyRegistry, value);
      }
      break;
    default:
      throw new Incident("UnexpectedType", {value, type: typeof  value});
  }
}

namespace chars {
  export const DIGIT_ZERO: Uint16 = 0x30;
  export const DIGIT_NINE: Uint16 = 0x39;
  export const DOT: Uint16 = 0x2e;
  export const PLUS: Uint16 = 0x2b;
  export const MINUS: Uint16 = 0x2d;
  export const LOWER_E: Uint16 = 0x65;
}
