import { mt64CharFromValue, Mt64ReadableStream } from "@eternalfest/mt64";
import { Incident } from "incident";
import { Float64, Sint32, Uint2, Uint16, UintSize } from "semantic-types";

import { KeyRegistry } from "./key-registry.js";
import { MtbonValue } from "./types.js";

export function parse(text: string): MtbonValue {
  const buffer: Buffer = Buffer.from(text);
  const registry: KeyRegistry = new KeyRegistry();
  const stream: Mt64ReadableStream = new Mt64ReadableStream(buffer);
  return readAny(stream, registry);
}

function readSint32(stream: Mt64ReadableStream): Sint32 {
  const tagStart: Uint2 = stream.readUint32Bits(2) as Uint2;
  if (tagStart === 0b11) {
    if (stream.readBoolBits()) {
      return -readSint32(stream);
    }
    const hashHighBits: boolean = stream.readBoolBits();
    const lowBits: Uint16 = stream.readUint32Bits(16);
    const highBits: number = hashHighBits ? stream.readUint32Bits(16) : 0;
    return (highBits << 16) | lowBits;
  } else {
    return stream.readUint32Bits(2 * (tagStart + 1));
  }
}

function readFloat64(stream: Mt64ReadableStream): Float64 {
  if (stream.readBoolBits()) {
    if (stream.readBoolBits()) {
      return stream.readBoolBits() ? -Infinity : Infinity;
    } else {
      return NaN;
    }
  }
  const strLen: UintSize = stream.readUint32Bits(5);
  let str: string = "";
  for (let i: UintSize = 0; i < strLen; i++) {
    const code: number = stream.readUint32Bits(4);
    switch (code) {
      case 10:
        str += ".";
        break;
      case 11:
        str += "+";
        break;
      case 12:
        str += "-";
        break;
      default:
        if (code < 10) {
          str += code.toString(10);
        } else {
          str += "e";
        }
        break;
    }
  }
  return parseFloat(str);
}

function readList(stream: Mt64ReadableStream, keyRegistry: KeyRegistry): MtbonValue[] {
  const result: MtbonValue = [];
  for (;;) {
    if (stream.readBoolBits()) {
      if (stream.readBoolBits()) {
        break;
      }
      const nullSequenceLength: UintSize = readSint32(stream);
      for (let i: number = 0; i < nullSequenceLength; i++) {
        result.push(null);
      }
    } else {
      result.push(readAny(stream, keyRegistry));
    }
  }
  return result;
}

function readLatin1String(stream: Mt64ReadableStream): string {
  const strLen: UintSize = readSint32(stream);
  // 6 bits: MT64
  // 7 bits: ASCII
  // 8 bits: latin1
  let charBits: number;
  if (stream.readBoolBits()) {
    charBits = stream.readBoolBits() ? 8 : 7;
  } else {
    charBits = 6;
  }
  let result: string = "";
  for (let i: number = 0; i < strLen; i++) {
    const code: number = stream.readUint32Bits(charBits);
    if (charBits === 6) {
      result += mt64CharFromValue(code)!;
    } else {
      result += String.fromCodePoint(code);
    }
  }
  return result;
}

function readDocument(stream: Mt64ReadableStream, keyRegistry: KeyRegistry) {
  const result: Record<string, MtbonValue> = Object.create(null);
  for (;;) {
    let key: string;
    if (stream.readBoolBits()) {
      if (stream.readBoolBits()) {
        break;
      }
      key = readLatin1String(stream);
      keyRegistry.add(key);
    } else {
      const id: UintSize = stream.readUint32Bits(keyRegistry.getIdBits());
      key = keyRegistry.getKey(id)!;
    }
    result[key] = readAny(stream, keyRegistry);
  }
  return result;
}

function readAny(stream: Mt64ReadableStream, keyRegistry: KeyRegistry): MtbonValue {
  const tagStart: Uint2 = stream.readUint32Bits(2) as Uint2;
  switch (tagStart) {
    case 0b00:
      return readSint32(stream);
    case 0b01:
      return readFloat64(stream);
    case 0b10:
      if (stream.readBoolBits()) {
        return readDocument(stream, keyRegistry);
      } else {
        return readList(stream, keyRegistry);
      }
    case 0b11:
      if (stream.readBoolBits()) {
        return stream.readBoolBits() ? null : readLatin1String(stream);
      } else {
        return stream.readBoolBits();
      }
    default:
      throw new Incident("UnexpectedTagStart", {tagStart});
  }
}
