# Next

- **[Fix]** Fix bug when encoding `-0x80000000`.

# 2.0.0 (2021-07-23)

- **[Breaking change]** Drop `lib` prefix and `.js` extension from deep-imports.
- **[Internal]** Refactor project structure.

# 1.0.1 (2019-12-28)

- **[Fix]** Update dependencies.
- **[Internal]** Add test case.

# 1.0.0 (2019-05-15)

- **[Fix]** Update dependencies.

# 0.1.3 (2018-12-03)

- **[Fix]** Update dependencies.

# 0.1.2 (2018-10-12)

- **[Fix]** Mark `@eternalfest/mtbon` as a runtime dependency.

# 0.1.1 (2018-10-12)

- **[Fix]** Extend the id bits earlier in key registry to match the original algorithm.

# 0.1.0 (2018-10-11)

- **[Feature]** First release.
