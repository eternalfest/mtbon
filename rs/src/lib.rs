mod cache;
mod de;
mod error;
mod io;
mod mt64;
mod reader;
mod ser;
mod writer;

#[cfg(test)]
mod tests;

use serde::{Deserialize, Serialize};
use std::io::{Read, Write};

pub use error::Error;
pub type Result<T> = std::result::Result<T, Error>;

#[inline]
pub fn to_writer<W, T>(writer: W, value: &T) -> Result<()>
where
  W: Write,
  T: Serialize + ?Sized,
{
  let mut ctx = writer::MtbonCtx::new(writer);
  value.serialize(&mut ser::Serializer(ctx.begin()))?;
  ctx.finish()?;
  Ok(())
}

#[inline]
pub fn to_vec<T: Serialize + ?Sized>(value: &T) -> Result<Vec<u8>> {
  let mut vec = Vec::new();
  to_writer(&mut vec, value)?;
  Ok(vec)
}

#[inline]
pub fn to_string<T: Serialize + ?Sized>(value: &T) -> Result<String> {
  // SAFETY: we never emit non-utf8 bytes.
  to_vec(value).map(|bytes| unsafe { String::from_utf8_unchecked(bytes) })
}

fn from_read<'de, T: Deserialize<'de>, R: io::Read>(read: R) -> Result<T> {
  let mut reader = reader::MtbonReader::new(read);
  let de = de::Deserializer::begin(&mut reader)?;
  T::deserialize(de)
}

#[inline]
pub fn from_slice<'de, T: Deserialize<'de>>(bytes: &[u8]) -> Result<T> {
  from_read(io::SliceRead::new(bytes))
}

#[inline]
pub fn from_str<'de, T: Deserialize<'de>>(s: &str) -> Result<T> {
  from_slice(s.as_bytes())
}

#[inline]
pub fn from_reader<'de, T: Deserialize<'de>, R: Read>(reader: R) -> Result<T> {
  from_read(io::IoRead::new(reader))
}
