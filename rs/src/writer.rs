use std::convert::TryFrom;
use std::io::Write;

use crate::cache::WriterCache;
use crate::error::ErrorKind;
use crate::mt64::{self, Mt64Writer};
use crate::Result;

pub struct MtbonCtx<'s, W> {
  writer: Mt64Writer<W>,
  key_cache: WriterCache<'s>,
}

impl<'s, W: Write> MtbonCtx<'s, W> {
  pub fn new(writer: W) -> Self {
    Self {
      writer: Mt64Writer::new(writer),
      key_cache: WriterCache::default(),
    }
  }

  pub fn begin(&mut self) -> MtbonWriter<'_, 's, W, ObjFlavor<0>> {
    MtbonWriter {
      ctx: self,
      flavor: ObjFlavor(()),
    }
  }

  pub fn finish(self) -> Result<W> {
    self.writer.finish()
  }

  fn write_i16(&mut self, mut v: i16) -> Result<()> {
    if v < 0 {
      self.writer.write::<3>(0b111)?;
      match v.checked_abs() {
        Some(abs) => v = abs,
        None => return self.write_i32(-i32::from(v)),
      }
    }

    self.write_u16(v as u16)
  }

  fn write_u16(&mut self, v: u16) -> Result<()> {
    if v < 1 << 2 {
      self.writer.write::<2>(0b00)?;
      self.writer.write::<2>(v.into())
    } else if v < 1 << 4 {
      self.writer.write::<2>(0b01)?;
      self.writer.write::<4>(v.into())
    } else if v < 1 << 6 {
      self.writer.write::<2>(0b10)?;
      self.writer.write::<6>(v.into())
    } else {
      self.writer.write::<4>(0b1100)?;
      self.writer.write::<16>(v.into())
    }
  }

  fn write_i32(&mut self, mut v: i32) -> Result<()> {
    if v < 0 {
      match v.checked_abs() {
        Some(abs) => {
          self.writer.write::<3>(0b111)?;
          v = abs;
        }
        None => panic!("can't write i32::MIN as int"),
      }
    }

    match u16::try_from(v) {
      Ok(v) => self.write_u16(v),
      Err(_) => {
        self.writer.write::<4>(0b1101)?;
        self.writer.write::<16>((v as u32) & 0xFFFF)?;
        self.writer.write::<16>((v as u32) >> 16)
      }
    }
  }

  fn write_finite_nonzero_f64(&mut self, v: f64) -> Result<()> {
    let mut buf = [0; 31];
    let buf = write_short_float(&mut buf, v);
    // We know `buf.len() < 32`, so we can't overflow the length.
    self.writer.write::<5>(buf.len() as u32)?;

    for c in buf {
      let nibble = match c {
        b'0'..=b'9' => (c - b'0').into(),
        b'.' => 10,
        b'+' => 11,
        b'-' => 12,
        b'e' => 13,
        _ => panic!("unexpected float char {}", c),
      };
      self.writer.write::<4>(nibble)?;
    }

    Ok(())
  }

  fn write_latin1_str(&mut self, v: &str) -> Result<()> {
    // Determine the number of bits per char...
    let mut bits = 6;
    let mut len = 0usize;
    for c in v.chars() {
      len += 1;
      match u8::try_from(u32::from(c)) {
        // 6 bits: MT64
        Ok(c) if mt64::char_to_bits(c).is_some() => continue,
        // 7 bits: ASCII
        Ok(0..=0x7F) => bits = std::cmp::max(bits, 7),
        // 8 bits: Latin1
        Ok(_) => bits = std::cmp::max(bits, 8),
        Err(_) => return Err(ErrorKind::UnrepresentableChar(c).into()),
      }
    }

    // ...write the length of the string...
    match i32::try_from(len) {
      Ok(len) => self.write_i32(len)?,
      Err(_) => return Err(ErrorKind::StringTooBig(len).into()),
    }

    // ...then write all chars.
    match bits {
      6 => {
        self.writer.write::<1>(0b0)?;
        for c in v.as_bytes() {
          match mt64::char_to_bits(*c) {
            Some(b) => self.writer.write::<6>(b.into())?,
            // SAFETY:
            // This can never happen, as `bits == 6` means that
            // we checked that all chars are in the MT64 charset.
            None => unsafe { std::hint::unreachable_unchecked() },
          }
        }
      }
      7 => {
        self.writer.write::<2>(0b10)?;
        for c in v.as_bytes() {
          self.writer.write::<7>((*c).into())?;
        }
      }
      _ => {
        self.writer.write::<2>(0b11)?;
        for c in v.chars() {
          self.writer.write::<8>(c.into())?;
        }
      }
    }
    Ok(())
  }
}

pub trait MtbonFlavor: 'static {
  fn flush_nulls<W: Write>(&mut self, ctx: &mut MtbonCtx<'_, W>, has_next: bool) -> Result<()>;

  fn emit_null<W: Write>(&mut self, ctx: &mut MtbonCtx<'_, W>) -> Result<()>;

  const NESTING_LEVEL: usize;
}

pub struct ArrayFlavor<const NEST: usize> {
  pending_nulls: i32,
}

impl<const NEST: usize> MtbonFlavor for ArrayFlavor<NEST> {
  fn flush_nulls<W: Write>(&mut self, ctx: &mut MtbonCtx<'_, W>, has_next: bool) -> Result<()> {
    if self.pending_nulls > 0 {
      ctx.writer.write::<2>(0b10)?;
      ctx.write_i32(self.pending_nulls)?;
      self.pending_nulls = 0;
    }
    if has_next {
      ctx.writer.write::<1>(0b0)?;
    }
    Ok(())
  }

  fn emit_null<W: Write>(&mut self, _ctx: &mut MtbonCtx<'_, W>) -> Result<()> {
    self.pending_nulls = self
      .pending_nulls
      .checked_add(1)
      .ok_or(ErrorKind::TooManyNulls)?;
    Ok(())
  }

  const NESTING_LEVEL: usize = NEST;
}

pub struct ObjFlavor<const NEST: usize>(());

impl<const NEST: usize> MtbonFlavor for ObjFlavor<NEST> {
  fn flush_nulls<W: Write>(&mut self, _ctx: &mut MtbonCtx<'_, W>, _has_next: bool) -> Result<()> {
    Ok(())
  }

  fn emit_null<W: Write>(&mut self, ctx: &mut MtbonCtx<'_, W>) -> Result<()> {
    ctx.writer.write::<4>(0b1111)
  }

  const NESTING_LEVEL: usize = NEST;
}

pub struct MtbonWriter<'ctx, 's, W, F> {
  ctx: &'ctx mut MtbonCtx<'s, W>,
  flavor: F,
}

impl<'ctx, 's, W, F> MtbonWriter<'ctx, 's, W, F>
where
  F: MtbonFlavor,
  W: Write,
{
  fn write_tag<const NBITS: u8>(&mut self, tag: u32) -> Result<()> {
    self.flavor.flush_nulls(self.ctx, true)?;
    self.ctx.writer.write::<NBITS>(tag)
  }

  pub fn emit_null(&mut self) -> Result<()> {
    self.flavor.emit_null(self.ctx)
  }

  pub fn emit_bool(&mut self, v: bool) -> Result<()> {
    self.write_tag::<4>(0b1100 + u32::from(v))
  }

  pub fn emit_i16(&mut self, v: i16) -> Result<()> {
    self.write_tag::<2>(0b00)?;
    self.ctx.write_i16(v)
  }

  pub fn emit_u16(&mut self, v: u16) -> Result<()> {
    self.write_tag::<2>(0b00)?;
    self.ctx.write_u16(v)
  }

  pub fn emit_i32(&mut self, v: i32) -> Result<()> {
    if v == i32::MIN {
      // `i32::MIN` can't be written as a int, so write it as a float.
      self.write_tag::<3>(0b010)?;
      self.ctx.write_finite_nonzero_f64(v.into())
    } else {
      self.write_tag::<2>(0b00)?;
      self.ctx.write_i32(v)
    }
  }

  pub fn emit_f64(&mut self, v: f64) -> Result<()> {
    use std::num::FpCategory;

    match v.classify() {
      FpCategory::Nan => self.write_tag::<4>(0b0110),
      FpCategory::Infinite => {
        let tag = if v.is_sign_positive() {
          0b01110
        } else {
          0b01111
        };
        self.write_tag::<5>(tag)
      }
      _ if v > i32::MIN.into() && v <= i32::MAX.into() && v.fract() == 0.0 => {
        // Write float as an int if possible.
        self.write_tag::<2>(0b00)?;
        self.ctx.write_i32(v as i32)
      }
      _ => {
        self.write_tag::<3>(0b010)?;
        self.ctx.write_finite_nonzero_f64(v)
      }
    }
  }

  pub fn emit_str(&mut self, v: &str) -> Result<()> {
    self.write_tag::<4>(0b1110)?;
    self.ctx.write_latin1_str(v)
  }

  pub fn emit_key(&mut self, key: &str) -> Result<()> {
    let cached = self.ctx.key_cache.cache_key(key);
    if cached.cached {
      self.write_tag::<1>(0b0)?;
      self.ctx.writer.write_dyn(cached.id_bits, cached.id)
    } else {
      self.write_tag::<2>(0b10)?;
      self.ctx.write_latin1_str(key)
    }
  }

  pub fn emit_borrowed_key(&mut self, key: &'s str) -> Result<()> {
    let cached = self.ctx.key_cache.cache_borrowed_key(key);
    if cached.cached {
      self.write_tag::<1>(0b0)?;
      self.ctx.writer.write_dyn(cached.id_bits, cached.id)
    } else {
      self.write_tag::<2>(0b10)?;
      self.ctx.write_latin1_str(key)
    }
  }

  pub fn begin_array<const NEST: usize>(
    &mut self,
    nested_fields: [&str; NEST],
  ) -> Result<MtbonWriter<'_, 's, W, ArrayFlavor<NEST>>> {
    for field in nested_fields {
      self.write_tag::<3>(0b101)?;
      self.emit_key(field)?;
    }

    self.write_tag::<3>(0b100)?;
    Ok(MtbonWriter {
      ctx: self.ctx,
      flavor: ArrayFlavor { pending_nulls: 0 },
    })
  }

  pub fn begin_object<const NEST: usize>(
    &mut self,
    nested_fields: [&str; NEST],
  ) -> Result<MtbonWriter<'_, 's, W, ObjFlavor<NEST>>> {
    for field in nested_fields {
      self.write_tag::<3>(0b101)?;
      self.emit_key(field)?;
    }

    self.write_tag::<3>(0b101)?;
    Ok(MtbonWriter {
      ctx: self.ctx,
      flavor: ObjFlavor(()),
    })
  }

  pub fn end_compound(&mut self) -> Result<()> {
    self.flavor.flush_nulls(self.ctx, false)?;
    for _ in 0..F::NESTING_LEVEL {
      self.ctx.writer.write::<2>(0b11)?;
    }
    self.ctx.writer.write::<2>(0b11)
  }
}

fn write_short_float(buf: &mut [u8; 31], v: f64) -> &[u8] {
  // We don't care about special casing 0; because this function
  // will only be called with non-zero floats.

  let mut fmt = dtoa::Buffer::new();
  let fmt = fmt.format(v);

  let len = fmt.len();
  let mut buf = &mut buf[..len];
  buf.copy_from_slice(fmt.as_bytes());

  fn count_zeroes<'a>(it: impl Iterator<Item = &'a u8>) -> usize {
    it.take_while(|b| **b == b'0').count()
  }

  // If the float is an integer, remove the `.0` and replace
  // trailing zeroes by a positive exponent.
  // Ex.: 1200.0 => 12e2; -1200 => -12e2
  if buf.ends_with(b".0") {
    debug_assert!(!buf.contains(&b'e'));

    buf = &mut buf[..len - 2];
    let zeroes = count_zeroes(buf.iter().rev());
    if zeroes > 2 {
      let mut exp = itoa::Buffer::new();
      let exp = exp.format(zeroes);

      let end = buf.len() - zeroes + 1;
      buf = &mut buf[..end + exp.len()];
      buf[end - 1] = b'e';
      buf[end..].copy_from_slice(exp.as_bytes());
    }

    return buf;
  }

  // If the float is a small number, remove the first `0` and
  // replace leading fractional zeroes by a negative exponent.
  // Ex.: 0.123 => .123; 0.0001 => 1e-4; -0.123 => -.123; -0.0001 => -1e-4
  let start = usize::from(v.is_sign_negative());
  if buf[start..].starts_with(b"0.") {
    debug_assert!(!buf.contains(&b'e'));

    let zeroes = count_zeroes(buf[start + 2..].iter());
    if zeroes < 3 {
      buf[start] = b'-';
      return &buf[1..];
    } else {
      buf.copy_within(zeroes + start + 2.., start);
      let end = buf.len() - zeroes;
      let mut exp = itoa::Buffer::new();
      let exp = exp.format(buf.len() - 2 - start);
      buf = &mut buf[..end + exp.len()];
      buf[end - 2] = b'e';
      buf[end - 1] = b'-';
      buf[end..].copy_from_slice(exp.as_bytes());
      return buf;
    }
  }
  
  buf
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn short_floats() {
    fn test_repr(v: f64, repr: &str) {
      let mut buf = [0; 31];
      let actual = std::str::from_utf8(write_short_float(&mut buf, v)).unwrap();
      assert_eq!(actual, repr);
    }

    test_repr(0.0, "0");
    test_repr(10.0, "10");
    test_repr(200.0, "200");
    test_repr(3000.0, "3e3");
    test_repr(4500000.0, "45e5");
    test_repr(-10.0, "-10");
    test_repr(-200.0, "-200");
    test_repr(-3000.0, "-3e3");
    test_repr(-4500000.0, "-45e5");
    test_repr(-0.1, "-.1");
    test_repr(-0.02, "-.02");
    test_repr(-0.003, "-.003");
    test_repr(-0.000045, "-45e-6");
  }
}
