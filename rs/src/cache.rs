use beef::Cow;
use std::collections::hash_map::{Entry, HashMap};
use std::convert::TryFrom;

#[derive(Default)]
pub struct ReaderCache {
  keys: StorageById<String>,
}

impl ReaderCache {
  pub fn add_key(&mut self, key: String) -> u32 {
    self.keys.store(key)
  }

  pub fn id_bits(&self) -> u8 {
    self.keys.id_bits
  }

  pub fn get_key(&self, id: u32) -> Option<&str> {
    self.keys.get(id).map(|key| key.as_str())
  }
}

#[derive(Default)]
pub struct WriterCache<'s> {
  keys: HashMap<Cow<'s, str>, u32>,
  ids: StorageById<()>,
}

impl<'s> WriterCache<'s> {
  pub fn cache_borrowed_key(&mut self, key: &'s str) -> CachedKey {
    match self.keys.entry(Cow::borrowed(key)) {
      Entry::Occupied(e) => self.ids.make_key(true, *e.get()),
      Entry::Vacant(e) => {
        let id = self.ids.store(());
        self.ids.make_key(false, *e.insert(id))
      }
    }
  }

  pub fn cache_key(&mut self, key: &str) -> CachedKey {
    if let Some(id) = self.keys.get(key) {
      self.ids.make_key(true, *id)
    } else {
      let id = self.ids.store(());
      self.keys.insert(Cow::owned(key.to_owned()), id);
      self.ids.make_key(false, id)
    }
  }
}

pub struct CachedKey {
  pub cached: bool,
  pub id: u32,
  pub id_bits: u8,
}

#[derive(Default)]
struct StorageById<V> {
  by_id: Vec<V>,
  id_bits: u8,
}

impl<V> StorageById<V> {
  fn store(&mut self, value: V) -> u32 {
    // TODO: return a result?
    let id = u32::try_from(self.by_id.len()).expect("overflow: too many cached keys");
    self.by_id.push(value);
    if self.by_id.len() == 1 << self.id_bits {
      self.id_bits += 1;
    }
    id
  }

  fn get(&self, id: u32) -> Option<&V> {
    self.by_id.get(id as usize)
  }

  fn make_key(&self, cached: bool, id: u32) -> CachedKey {
    CachedKey {
      cached,
      id,
      id_bits: self.id_bits,
    }
  }
}
