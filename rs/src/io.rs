use std::io::{Read as StdRead, Result};

// When read as MT64, this is 0b111111.
const EOF_BYTE: u8 = b'_';

/// Replacement trait for `StdRead` with simpler operations.
/// Completely ignores EOF and pretends there is an endless supply
/// of `EOF_BYTE` at the end of the stream.
pub trait Read {
  fn next(&mut self) -> Result<u8>;

  fn skip(&mut self, bytes: usize) -> Result<()>;
}

pub struct IoRead<R: StdRead> {
  reader: R,
}

impl<R: StdRead> IoRead<R> {
  pub fn new(reader: R) -> Self {
    Self { reader }
  }
}

impl<R: StdRead> Read for IoRead<R> {
  #[inline]
  fn next(&mut self) -> Result<u8> {
    let mut byte = [EOF_BYTE];
    // If EOF, do nothing and return zero.
    let _ = self.reader.read(&mut byte)?;
    Ok(byte[0])
  }

  #[inline]
  fn skip(&mut self, mut bytes: usize) -> Result<()> {
    const BUF_SIZE: usize = 256;

    let mut buffer = [0; BUF_SIZE];
    while bytes > 0 {
      let buf = &mut buffer[..std::cmp::min(bytes, BUF_SIZE)];
      let consumed = self.reader.read(buf)?;
      if consumed == 0 {
        break; // Abort on EOF.
      }
      bytes -= consumed;
    }
    Ok(())
  }
}

pub struct SliceRead<'a> {
  bytes: &'a [u8],
  pos: usize,
}

impl<'a> SliceRead<'a> {
  pub fn new(bytes: &'a [u8]) -> Self {
    Self { bytes, pos: 0 }
  }
}

impl<'a> Read for SliceRead<'a> {
  #[inline]
  fn next(&mut self) -> Result<u8> {
    match self.bytes.get(self.pos) {
      Some(byte) => {
        self.pos += 1;
        Ok(*byte)
      }
      None => Ok(EOF_BYTE),
    }
  }

  #[inline]
  fn skip(&mut self, bytes: usize) -> Result<()> {
    self.pos = self.pos.saturating_add(bytes);
    Ok(())
  }
}
