use std::io::Write;

use crate::io::Read;
use crate::{error::ErrorKind, Result};

pub enum StringKind {
  Mt64,
  Ascii,
  Latin1,
}

impl StringKind {
  pub fn bits_per_char(&self) -> u8 {
    match self {
      Self::Mt64 => 6,
      Self::Ascii => 7,
      Self::Latin1 => 8,
    }
  }
}

const BITS_PER_BYTE: u8 = 6;
const CHARSET: [u8; 64] = *b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";

macro_rules! const_type {
  ($name:ident($ty:ty)) => {
    struct $name<const N: $ty>;

    impl<const N: $ty> From<$name<N>> for $ty {
      fn from(_: $name<N>) -> Self {
        N
      }
    }
  };
}

const_type!(ConstU8(u8));
const_type!(ConstU64(u64));

pub struct Mt64Writer<W> {
  inner: W,
  bit_pos: u8,
  next_bits: u8,
}

impl<W: Write> Mt64Writer<W> {
  pub fn new(writer: W) -> Self {
    Self {
      inner: writer,
      bit_pos: 0,
      next_bits: 0,
    }
  }

  fn write_inner(&mut self, nbits: impl Into<u8>, bits: u32) -> Result<()> {
    let mut nbits = nbits.into();
    assert!(nbits <= 32, "can't write more that 32 bits at once");

    while nbits > 0 {
      let available = BITS_PER_BYTE - self.bit_pos;
      let consumed = std::cmp::min(available, nbits);
      let chunk = (bits >> (nbits - consumed)) & ((1 << consumed) - 1);

      self.next_bits |= (chunk as u8) << (available - consumed);
      nbits -= consumed;
      self.bit_pos += consumed;

      if self.bit_pos == BITS_PER_BYTE {
        self.write_byte(self.next_bits)?;
        self.next_bits = 0;
        self.bit_pos = 0;
      }
    }

    Ok(())
  }

  pub fn write<const NBITS: u8>(&mut self, bits: u32) -> Result<()> {
    self.write_inner(ConstU8::<NBITS>, bits)
  }

  pub fn write_dyn(&mut self, nbits: u8, bits: u32) -> Result<()> {
    self.write_inner(nbits, bits)
  }

  #[inline(always)]
  fn write_byte(&mut self, bits: u8) -> Result<()> {
    let byte = bits_to_char(bits);
    match self.inner.write_all(&[byte]) {
      Ok(_) => Ok(()),
      Err(err) => Err(ErrorKind::Io(err).into()),
    }
  }

  pub fn finish(mut self) -> Result<W> {
    if self.bit_pos != 0 {
      self.write_byte(self.next_bits)?;
    }
    Ok(self.inner)
  }
}

pub struct Mt64Reader<R> {
  inner: R,
  bit_pos: u8,
  next_bits: u8,
}

impl<R: Read> Mt64Reader<R> {
  pub fn new(reader: R) -> Self {
    Self {
      inner: reader,
      bit_pos: BITS_PER_BYTE,
      next_bits: 0,
    }
  }

  fn read_inner(&mut self, nbits: impl Into<u8>) -> Result<u32> {
    let mut nbits = nbits.into();
    assert!(nbits <= 32, "can't read more that 32 bits at once");

    let mut res = 0u32;
    while nbits > 0 {
      let last_bit = self.bit_pos + nbits;
      if last_bit < BITS_PER_BYTE {
        let cur = (self.next_bits >> (BITS_PER_BYTE - last_bit)) & ((1 << nbits) - 1);
        res = (res << nbits) + u32::from(cur);
        self.bit_pos += nbits;
        break;
      } else {
        let shift = BITS_PER_BYTE - self.bit_pos;
        let cur = self.next_bits & ((1 << shift) - 1);
        res = (res << shift) + u32::from(cur);
        nbits -= shift;
        self.next_bits = self.read_byte()?;
        self.bit_pos = 0;
      }
    }

    Ok(res)
  }

  fn skip_inner(&mut self, nbits: impl Into<u64>) -> Result<()> {
    let nbits = nbits.into();

    match nbits.checked_sub((BITS_PER_BYTE - self.bit_pos).into()) {
      None => self.bit_pos += nbits as u8,
      Some(nbits) => {
        let to_skip = nbits / u64::from(BITS_PER_BYTE);
        let extra = nbits % u64::from(BITS_PER_BYTE);
        // TODO: overflow?
        self.next_bits = self
          .inner
          .skip(to_skip as usize)
          .and_then(|_| self.inner.next())
          .map_err(|err| crate::Error::from(ErrorKind::Io(err)))?;
        self.bit_pos = extra as u8;
      }
    }

    Ok(())
  }

  #[inline]
  pub fn read<const NBITS: u8>(&mut self) -> Result<u32> {
    self.read_inner(ConstU8::<NBITS>)
  }

  #[inline]
  pub fn read_dyn(&mut self, nbits: u8) -> Result<u32> {
    self.read_inner(nbits)
  }

  #[inline]
  pub fn skip<const NBITS: u64>(&mut self) -> Result<()> {
    self.skip_inner(ConstU64::<NBITS>)
  }

  #[inline]
  pub fn skip_dyn(&mut self, nbits: u64) -> Result<()> {
    self.skip_inner(nbits)
  }

  #[inline(always)]
  fn read_byte(&mut self) -> Result<u8> {
    match self.inner.next() {
      Ok(byte) => char_to_bits(byte).ok_or_else(|| ErrorKind::InvalidMt64(byte).into()),
      Err(err) => Err(ErrorKind::Io(err).into()),
    }
  }
}

#[inline(always)]
pub fn bits_to_char(bits: u8) -> u8 {
  CHARSET[usize::from(bits)]
}

const CHAR_TO_BITS: [u8; 256] = {
  let mut bits = [0xFF; 256];
  let mut i = 0u8;
  while (i as usize) < CHARSET.len() {
    bits[CHARSET[i as usize] as usize] = i;
    i += 1;
  }
  bits
};

#[inline(always)]
pub fn char_to_bits(c: u8) -> Option<u8> {
  let bits = CHAR_TO_BITS[usize::from(c)];
  if bits == 0xFF {
    None
  } else {
    Some(bits)
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  const CASES: &[(&str, &str)] = &[
    ("", ""),
    ("0", "a"),
    ("1", "G"),
    ("00", "a"),
    ("01", "q"),
    ("11", "W"),
    ("0001", "e"),
    ("00001", "c"),
    ("000000", "a"),
    ("000001", "b"),
    ("000010", "c"),
    ("000011", "d"),
    ("0000001", "aG"),
    ("11111111/11111111/11111111/11111111", "_____W"),
    ("10000/0100/000100000100/0001", "GGGGG"),
  ];

  #[test]
  fn writer() {
    for (bits, expected) in CASES {
      let mut writer = Mt64Writer::new(Vec::new());

      for chunk in bits.split('/') {
        let mut val = 0;
        for c in chunk.chars() {
          val *= 2;
          match c {
            '0' => (),
            '1' => val += 1,
            _ => panic!("unexpected char: {}", c),
          }
        }
        writer.write_dyn(chunk.len() as u8, val).unwrap();
      }

      let actual = String::from_utf8(writer.finish().unwrap()).unwrap();
      assert_eq!(actual, *expected, "for {}", bits);
    }
  }

  #[test]
  fn reader() {
    for (expected, mtbon) in CASES {
      let mut reader = Mt64Reader::new(crate::io::SliceRead::new(mtbon.as_bytes()));

      let mut actual = Vec::new();
      for chunk in expected.split('/') {
        if !actual.is_empty() {
          actual.push(b'/');
        }

        // Read bits and convert to binary string
        let start = actual.len();
        let mut cur = reader.read_dyn(chunk.len() as u8).unwrap();
        for _ in 0..chunk.len() {
          actual.push(b"01"[(cur & 1) as usize]);
          cur >>= 1;
        }
        actual[start..].reverse();
      }

      let actual = std::str::from_utf8(&actual).unwrap();
      assert_eq!(actual, *expected, "for {}", mtbon);
    }
  }
}
