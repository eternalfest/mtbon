use std::fmt::{Debug, Write};

use serde::{Deserialize, Serialize};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

/// Bare-bones type for MTBON values, with guaranteed
/// ordering of keys in objects.
#[derive(Debug, Deserialize, Serialize)]
#[serde(untagged)]
enum Value {
  Array(Vec<Value>),
  #[serde(with = "key_value_pairs")]
  Object(Vec<(String, Value)>),
  Null(()),
  Bool(bool),
  String(String),
  Number(f64),
}

// Manual impl to handle NaNs
impl std::cmp::PartialEq for Value {
  fn eq(&self, other: &Self) -> bool {
    match (self, other) {
      (Value::Null(()), Value::Null(())) => true,
      (Value::Bool(a), Value::Bool(b)) => a == b,
      (Value::String(a), Value::String(b)) => a == b,
      (Value::Number(a), Value::Number(b)) => a == b || (a.is_nan() && b.is_nan()),
      (Value::Array(a), Value::Array(b)) => a == b,
      (Value::Object(a), Value::Object(b)) => a == b,
      _ => false,
    }
  }
}

mod key_value_pairs {
  use super::Value;
  use serde::de::{Deserializer, MapAccess, Visitor};
  use serde::ser::{SerializeMap, Serializer};
  use std::fmt::{self, Formatter};

  pub(super) fn serialize<S: Serializer>(
    pairs: &[(String, Value)],
    ser: S,
  ) -> Result<S::Ok, S::Error> {
    let mut obj = ser.serialize_map(Some(pairs.len()))?;
    for (key, value) in pairs {
      obj.serialize_entry(key, value)?;
    }
    obj.end()
  }

  pub(super) fn deserialize<'de, D: Deserializer<'de>>(
    de: D,
  ) -> Result<Vec<(String, Value)>, D::Error> {
    struct PairVisitor;

    impl<'de> Visitor<'de> for PairVisitor {
      type Value = Vec<(String, Value)>;

      fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
        formatter.write_str("an object")
      }

      fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
        let mut pairs = Vec::new();
        while let Some(entry) = map.next_entry()? {
          pairs.push(entry);
        }
        Ok(pairs)
      }
    }

    de.deserialize_map(PairVisitor)
  }
}

struct TestCase {
  raw: String,
  value: Value,
  mtbon: Vec<String>,
}

fn load_test_cases(data: &str) -> Result<Vec<TestCase>> {
  #[derive(Deserialize)]
  #[serde(untagged)]
  enum RawMtbon {
    One(String),
    Many(Vec<String>),
  }

  #[derive(Deserialize)]
  struct RawTestCase {
    js: String,
    mtbon: RawMtbon,
  }

  // We need to use json5 instead of json to properly handle NaNs and infinities.
  json5::from_str::<Vec<RawTestCase>>(data)?
    .into_iter()
    .map(|mut test_case| {
      // Handle `({ ... })`
      if test_case.js.starts_with('(') && test_case.js.ends_with(')') {
        test_case.js.pop();
        test_case.js.remove(0);
      }

      Ok(TestCase {
        value: json5::from_str(&test_case.js)?,
        mtbon: match test_case.mtbon {
          RawMtbon::One(s) => vec![s],
          RawMtbon::Many(s) => s,
        },
        raw: test_case.js,
      })
    })
    .collect()
}

macro_rules! include_test_file {
  ($path:literal) => {
    include_str!(concat!("../../test-resources/", $path))
  };
}

#[test]
fn serialize() -> Result<()> {
  let test_cases = [
    load_test_cases(include_test_file!("bidirectional.json"))?,
    load_test_cases(include_test_file!("emit-only.json"))?,
  ];

  for test_case in test_cases.iter().flatten() {
    let mtbon = crate::to_string(&test_case.value)?;

    if !test_case.mtbon.contains(&mtbon) {
      let mut msg = "\n".to_string();
      writeln!(msg, "Serialization failed for {}", test_case.raw)?;
      writeln!(msg, "- actual  : {}", mtbon)?;
      for expected in &test_case.mtbon {
        writeln!(msg, "- expected: {}", expected)?;
      }
      panic!("{}", msg);
    }
  }

  Ok(())
}

#[test]
fn deserialize() -> Result<()> {
  let test_cases = [
    load_test_cases(include_test_file!("bidirectional.json"))?,
    load_test_cases(include_test_file!("emit-only.json"))?,
  ];

  for test_case in test_cases.iter().flatten() {
    for mtbon in &test_case.mtbon {
      let value: Value = crate::from_str(mtbon)?;

      if value != test_case.value {
        let mut msg = "\n".to_string();
        writeln!(msg, "Deserialization failed for {}", test_case.raw)?;
        writeln!(msg, "- actual  : {:?}", value)?;
        writeln!(msg, "- expected: {:?}", test_case.value)?;
        panic!("{}", msg);
      }
    }
  }

  Ok(())
}

#[test]
fn number_conversions() {
  fn test_conv<'de, T>(number: &str, mtbon: &str, expected: Option<T>)
  where
    T: Deserialize<'de> + Debug + PartialEq,
  {
    let decoded = crate::from_str::<T>(mtbon);
    let fail = match (&expected, decoded) {
      (Some(val), Ok(actual)) if val == &actual => return,
      (Some(_), failed) => failed,
      (None, Ok(actual)) => Ok(actual),
      (None, Err(_)) => return,
    };

    panic!(
      "expected {} for {:?} as {}, got {:?}",
      if fail.is_ok() { "failure" } else { "success" },
      number,
      std::any::type_name::<T>(),
      expected.ok_or(()),
    );
  }

  macro_rules! cases {
    ($($number:literal: { Ok($($ty_ok:ident)*) Err($($ty_err:ident)*) })*) => { $(
      let mtbon = crate::to_string(&$number).unwrap();
      let number = stringify!(number);
      $(
        test_conv::<$ty_ok>(number, &mtbon, Some($number as $ty_ok));
      )*
      $(
        test_conv::<$ty_err>(number, &mtbon, None);
      )*
    )* };
  }

  cases! {
    10u8: {
      Ok(u8 i8 u16 i16 u32 i32 u64 i64 u128 i128 f32 f64)
      Err()
    }
    -10i8: {
      Ok(i8 i16 i32 i64 i128 f32 f64)
      Err(u8 u16 u32 u64 u128)
    }
    1_000u16: {
      Ok(u16 i16 u32 i32 u64 i64 u128 i128 f32 f64)
      Err(u8 i8)
    }
    -1_000i16: {
      Ok(i16 i32 i64 i128 f32 f64)
      Err(u8 i8 u16 u32 u64 u128)
    }
    1_000_000u32: {
      Ok(u32 i32 u64 i64 u128 i128 f32 f64)
      Err(u8 i8 u16 i16)
    }
    -1_000_000i32: {
      Ok(i32 i64 i128 f32 f64)
      Err(u8 i8 u16 i16 u32 u64 u128)
    }
    1_000_000_000_000u64: {
      Ok(u64 i64 u128 i128 f32 f64)
      Err(u8 i8 u16 i16 u32 i32)
    }
    -1_000_000_000_000i64: {
      Ok(i64 i128 f32 f64)
      Err(u8 i8 u16 i16 u32 i32 u64 u128)
    }
    // Float representable as an integer.
    1e10f64: {
      Ok(u64 i64 u128 i128 f32 f64)
      Err(u8 i8 u16 i16 u32 i32)
    }
    // Float unrepresentable as an integer.
    1e250f64: {
      Ok(f32 f64)
      Err(u8 i8 u16 i16 u32 i32 u64 i64 u128 i128)
    }
  }

  // A int bigger than 2⁵³ cannot be represented in a i32 or f64.
  assert!(crate::to_string(&1_000_000_000_000_000_000u64).is_err());
}
