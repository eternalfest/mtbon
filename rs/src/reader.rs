use std::convert::TryFrom;

use crate::cache::ReaderCache;
use crate::error::{Error, ErrorKind};
use crate::io::Read;
use crate::mt64::{self, Mt64Reader, StringKind};
use crate::Result;

use serde::de::{Error as _, Expected, Unexpected};

pub enum Tag {
  Null,
  Bool(bool),
  Int(IntTag),
  Float(FloatTag),
  String(StringTag),
  Array(ArrayState),
  Object,
}

impl Tag {
  pub fn invalid_type(&self, exp: &dyn Expected) -> Error {
    let ty = match self {
      Tag::Null => "null",
      Tag::Bool(_) => "boolean",
      Tag::Int(_) => "integer",
      Tag::Float(_) => "float",
      Tag::String(_) => "string",
      Tag::Array(_) => "array",
      Tag::Object => "object",
    };

    Error::invalid_type(Unexpected::Other(ty), exp)
  }
}

pub struct IntTag {
  negative: bool,
  // If > 16, high and low 16-bits are swapped.
  nbits: u8,
}

pub struct StringTag {
  len: u32,
  kind: StringKind,
}

pub enum FloatTag {
  Infinity,
  Nan,
  Finite { len: u8 },
}

pub struct ArrayState {
  nulls: u32,
}

pub struct MtbonReader<R> {
  inner: Mt64Reader<R>,
  key_cache: ReaderCache,
  scratch: Vec<u8>,
}

impl<R: Read> MtbonReader<R> {
  #[inline]
  fn read<const NBITS: u8>(&mut self) -> Result<u32> {
    self.inner.read::<NBITS>()
  }

  #[inline]
  fn read_bool(&mut self) -> Result<bool> {
    self.inner.read::<1>().map(|b| b == 1)
  }
}

impl<R: Read> MtbonReader<R> {
  pub fn new(reader: R) -> Self {
    Self {
      inner: Mt64Reader::new(reader),
      key_cache: Default::default(),
      scratch: Vec::new(),
    }
  }

  fn parse_int_tag(&mut self) -> Result<IntTag> {
    let nbits = match self.read::<2>()? {
      0b11 => {
        if self.read_bool()? {
          let mut tag = self.parse_int_tag()?;
          tag.negative = !tag.negative;
          return Ok(tag);
        }
        let n = self.read::<1>()?;
        16 * (n + 1)
      }
      n => 2 * (n + 1),
    };

    Ok(IntTag {
      negative: false,
      nbits: nbits as u8,
    })
  }

  fn parse_string_tag(&mut self) -> Result<StringTag> {
    let tag = self.parse_int_tag()?;
    let len = self.parse_int(tag)?;
    let len = u32::try_from(len).map_err(|_| ErrorKind::InvalidNegativeLen(len))?;

    let kind = if self.read_bool()? {
      if self.read_bool()? {
        StringKind::Latin1
      } else {
        StringKind::Ascii
      }
    } else {
      StringKind::Mt64
    };

    Ok(StringTag { len, kind })
  }

  fn parse_float_tag(&mut self) -> Result<FloatTag> {
    Ok(if self.read_bool()? {
      if self.read_bool()? {
        FloatTag::Infinity
      } else {
        FloatTag::Nan
      }
    } else {
      FloatTag::Finite {
        len: self.read::<5>()? as u8,
      }
    })
  }

  pub fn parse_tag(&mut self) -> Result<Tag> {
    Ok(match self.read::<2>()? {
      0b00 => Tag::Int(self.parse_int_tag()?),
      0b01 => Tag::Float(self.parse_float_tag()?),
      0b10 => {
        if self.read_bool()? {
          Tag::Object
        } else {
          Tag::Array(ArrayState { nulls: 0 })
        }
      }
      _ => match self.read::<2>()? {
        0b00 => Tag::Bool(false),
        0b01 => Tag::Bool(true),
        0b10 => Tag::String(self.parse_string_tag()?),
        _ => Tag::Null,
      },
    })
  }

  pub fn parse_array_elem(&mut self, state: &mut ArrayState) -> Result<Option<Tag>> {
    if state.nulls > 0 {
      state.nulls -= 1;
      return Ok(Some(Tag::Null));
    }

    if self.read_bool()? {
      if self.read_bool()? {
        return Ok(None);
      }

      let tag = self.parse_int_tag()?;
      let nulls = self.parse_int(tag)?;
      state.nulls = u32::try_from(nulls).map_err(|_| ErrorKind::InvalidNegativeLen(nulls))?;
      return self.parse_array_elem(state);
    }

    self.parse_tag().map(Some)
  }

  fn parse_object_key_id(&mut self) -> Result<Option<u32>> {
    let id = if self.read_bool()? {
      if self.read_bool()? {
        return Ok(None);
      }

      let key_tag = self.parse_string_tag()?;
      let key = self.parse_latin1_string_owned(key_tag)?;
      self.key_cache.add_key(key)
    } else {
      self.inner.read_dyn(self.key_cache.id_bits())?
    };

    Ok(Some(id))
  }

  pub fn parse_object_key(&mut self) -> Result<Option<&str>> {
    match self.parse_object_key_id()? {
      Some(id) => self
        .key_cache
        .get_key(id)
        .ok_or_else(|| ErrorKind::UnknownKeyId(id).into())
        .map(Some),
      None => Ok(None),
    }
  }

  pub fn ignore(&mut self, tag: Tag) -> Result<()> {
    match tag {
      Tag::Null | Tag::Bool(_) => Ok(()),
      Tag::Int(tag) => self.inner.skip_dyn(tag.nbits.into()),
      Tag::Float(FloatTag::Infinity) => self.inner.skip::<1>(),
      Tag::Float(FloatTag::Nan) => Ok(()),
      Tag::Float(FloatTag::Finite { len }) => self.inner.skip_dyn(u64::from(len) * 4),
      Tag::String(tag) => {
        let bits = u64::from(tag.len) * u64::from(tag.kind.bits_per_char());
        self.inner.skip_dyn(bits)
      }
      Tag::Array(mut state) => {
        while let Some(tag) = self.parse_array_elem(&mut state)? {
          self.ignore(tag)?;
        }
        Ok(())
      }
      Tag::Object => {
        while self.parse_object_key_id()?.is_some() {
          let tag = self.parse_tag()?;
          self.ignore(tag)?;
        }
        Ok(())
      }
    }
  }

  pub fn parse_int(&mut self, tag: IntTag) -> Result<i32> {
    let mut raw: u32 = self.inner.read_dyn(tag.nbits)?;

    if tag.nbits > 16 {
      let (hi, lo) = (raw >> 16, raw & 0xFFFF);
      raw = (lo << 16) | hi;
    }

    if tag.negative {
      raw = raw.wrapping_neg();
    }

    Ok(raw as i32)
  }

  pub fn parse_float(&mut self, tag: FloatTag) -> Result<f64> {
    match tag {
      FloatTag::Infinity => {
        if self.read_bool()? {
          Ok(f64::NEG_INFINITY)
        } else {
          Ok(f64::INFINITY)
        }
      }
      FloatTag::Nan => Ok(f64::NAN),
      FloatTag::Finite { len } => {
        let len = usize::from(len);
        let mut buf = [0; 31];
        for b in buf.iter_mut().take(len) {
          *b = match self.read::<4>()? {
            c @ 0..=9 => b'0' + c as u8,
            10 => b'.',
            11 => b'+',
            12 => b'-',
            13 => b'e',
            _ => b'\0', // use an invalid char to cause an error later.
          }
        }

        // SAFETY: buf contains only ASCII chars.
        let buf = unsafe { std::str::from_utf8_unchecked(&buf[..len]) };
        buf.parse().map_err(|_| ErrorKind::InvalidFloat.into())
      }
    }
  }

  fn parse_string_raw<const LATIN1: bool>(&mut self, tag: StringTag) -> Result<()> {
    self.scratch.clear();
    self.scratch.reserve(tag.len as usize);

    match tag.kind {
      StringKind::Mt64 => {
        for _ in 0..tag.len {
          let c = mt64::bits_to_char(self.read::<6>()? as u8);
          self.scratch.push(c);
        }
      }
      StringKind::Ascii => {
        for _ in 0..tag.len {
          let c = self.read::<7>()? as u8;
          self.scratch.push(c)
        }
      }
      StringKind::Latin1 => {
        for _ in 0..tag.len {
          let c = self.read::<8>()? as u8;
          if LATIN1 && c > 0x7F {
            // The code point becomes two utf8 bytes.
            self.scratch.push(0b1100_0000 | (c >> 6));
            self.scratch.push(0b1000_0000 | (c & 0b0011_1111));
          } else {
            self.scratch.push(c);
          }
        }
      }
    }

    if LATIN1 {
      debug_assert!(
        std::str::from_utf8(&self.scratch).is_ok(),
        "invalid utf8 string; should never happen",
      );
    }

    Ok(())
  }

  pub fn parse_latin1_string(&mut self, tag: StringTag) -> Result<&str> {
    self.parse_string_raw::<true>(tag)?;

    // SAFETY: `self.scratch` contains valid utf8 data
    Ok(unsafe { std::str::from_utf8_unchecked(&self.scratch) })
  }

  pub fn parse_latin1_string_owned(&mut self, tag: StringTag) -> Result<String> {
    let old = std::mem::take(&mut self.scratch);
    self.parse_string_raw::<true>(tag)?;
    let buf = std::mem::replace(&mut self.scratch, old);

    // SAFETY: `self.scratch` contains valid utf8 data
    Ok(unsafe { String::from_utf8_unchecked(buf) })
  }

  pub fn parse_bytes(&mut self, tag: StringTag) -> Result<&[u8]> {
    self.parse_string_raw::<false>(tag)?;
    Ok(&self.scratch)
  }

  pub fn parse_bytes_owned(&mut self, tag: StringTag) -> Result<Vec<u8>> {
    let old = std::mem::take(&mut self.scratch);
    self.parse_string_raw::<false>(tag)?;
    Ok(std::mem::replace(&mut self.scratch, old))
  }
}
