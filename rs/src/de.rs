use crate::io::Read;
use crate::reader::{ArrayState, MtbonReader, StringTag, Tag};
use crate::{Error, Result};

use serde::de::{DeserializeSeed, Error as _, IntoDeserializer, Unexpected, Visitor};

pub struct Deserializer<'ctx, R> {
  reader: &'ctx mut MtbonReader<R>,
  tag: Tag,
}

impl<'ctx, R: Read> Deserializer<'ctx, R> {
  pub fn begin(reader: &'ctx mut MtbonReader<R>) -> Result<Self> {
    let tag = reader.parse_tag()?;
    Ok(Self { reader, tag })
  }
}

macro_rules! forward_to_deserialize_i64 {
  ($($fn_name:ident)*) => {
    $(
      #[inline]
      fn $fn_name<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
        self.deserialize_i64(vis)
      }
    )*
  }
}

impl<'de, 'ctx, R: Read> serde::Deserializer<'de> for Deserializer<'ctx, R> {
  type Error = Error;

  serde::forward_to_deserialize_any! {
    bool char str unit unit_struct identifier
  }

  fn deserialize_any<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::Null => vis.visit_unit(),
      Tag::Bool(b) => vis.visit_bool(b),
      Tag::Int(tag) => vis.visit_i32(self.reader.parse_int(tag)?),
      Tag::Float(tag) => vis.visit_f64(self.reader.parse_float(tag)?),
      Tag::String(tag) => vis.visit_str(self.reader.parse_latin1_string(tag)?),
      Tag::Array(array) => vis.visit_seq(SeqAccess {
        reader: self.reader,
        array,
      }),
      Tag::Object => vis.visit_map(MapAccess(self.reader)),
    }
  }

  forward_to_deserialize_i64! {
    deserialize_u8 deserialize_i8
    deserialize_u16 deserialize_i16
    deserialize_u32 deserialize_i32
    deserialize_u64
    deserialize_u128 deserialize_i128
  }

  fn deserialize_i64<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    let n: i64 = match self.tag {
      Tag::Int(tag) => self.reader.parse_int(tag)?.into(),
      Tag::Float(tag) => {
        const MAX_SAFE_INT: f64 = (1u64 << 52) as f64;

        let f = self.reader.parse_float(tag)?;
        if (-MAX_SAFE_INT..=MAX_SAFE_INT).contains(&f) && f.fract() == 0.0 {
          f as i64
        } else {
          return Err(Error::invalid_value(Unexpected::Float(f), &vis));
        }
      }
      tag => return Err(tag.invalid_type(&vis)),
    };

    vis.visit_i64(n)
  }

  fn deserialize_f32<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    self.deserialize_f64(vis)
  }

  fn deserialize_f64<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::Int(tag) => vis.visit_f64(self.reader.parse_int(tag)?.into()),
      Tag::Float(tag) => vis.visit_f64(self.reader.parse_float(tag)?),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_string<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::String(tag) => vis.visit_string(self.reader.parse_latin1_string_owned(tag)?),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_bytes<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::String(tag) => vis.visit_byte_buf(self.reader.parse_bytes_owned(tag)?),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_byte_buf<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::String(tag) => vis.visit_bytes(self.reader.parse_bytes(tag)?),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_option<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::Null => vis.visit_none(),
      _ => vis.visit_some(self),
    }
  }

  fn deserialize_newtype_struct<V: Visitor<'de>>(
    self,
    _name: &'static str,
    vis: V,
  ) -> Result<V::Value>
where {
    vis.visit_newtype_struct(self)
  }

  fn deserialize_seq<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::Array(array) => vis.visit_seq(SeqAccess {
        reader: self.reader,
        array,
      }),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_tuple<V: Visitor<'de>>(self, _len: usize, vis: V) -> Result<V::Value> {
    self.deserialize_seq(vis)
  }

  fn deserialize_tuple_struct<V: Visitor<'de>>(
    self,
    _name: &'static str,
    _len: usize,
    vis: V,
  ) -> Result<V::Value> {
    self.deserialize_seq(vis)
  }

  fn deserialize_map<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    match self.tag {
      Tag::Object => vis.visit_map(MapAccess(self.reader)),
      tag => Err(tag.invalid_type(&vis)),
    }
  }

  fn deserialize_struct<V: Visitor<'de>>(
    self,
    _name: &'static str,
    _fields: &'static [&'static str],
    vis: V,
  ) -> Result<V::Value> {
    self.deserialize_map(vis)
  }

  fn deserialize_enum<V: Visitor<'de>>(
    self,
    _name: &'static str,
    _variants: &'static [&'static str],
    vis: V,
  ) -> Result<V::Value> {
    match self.tag {
      Tag::String(tag) => {
        return vis.visit_enum(UnitEnumAccess {
          reader: self.reader,
          tag,
        })
      }
      Tag::Object => {
        let v = vis.visit_enum(EnumAccess(self.reader))?;
        if self.reader.parse_object_key()?.is_none() {
          return Ok(v);
        }
      }
      _ => (),
    }

    Err(self.tag.invalid_type(&"enum variant"))
  }

  fn deserialize_ignored_any<V: Visitor<'de>>(self, vis: V) -> Result<V::Value> {
    self.reader.ignore(self.tag)?;
    vis.visit_unit()
  }

  fn is_human_readable(&self) -> bool {
    false
  }
}

struct SeqAccess<'ctx, R> {
  reader: &'ctx mut MtbonReader<R>,
  array: ArrayState,
}

impl<'de, 'ctx, R: Read> serde::de::SeqAccess<'de> for SeqAccess<'ctx, R> {
  type Error = Error;

  fn next_element_seed<T: DeserializeSeed<'de>>(&mut self, seed: T) -> Result<Option<T::Value>> {
    match self.reader.parse_array_elem(&mut self.array)? {
      None => Ok(None),
      Some(elem) => seed
        .deserialize(Deserializer {
          reader: self.reader,
          tag: elem,
        })
        .map(Some),
    }
  }
}

struct MapAccess<'ctx, R>(&'ctx mut MtbonReader<R>);

impl<'de, 'ctx, R: Read> serde::de::MapAccess<'de> for MapAccess<'ctx, R> {
  type Error = Error;

  fn next_key_seed<K: DeserializeSeed<'de>>(&mut self, seed: K) -> Result<Option<K::Value>> {
    match self.0.parse_object_key()? {
      Some(key) => seed.deserialize(key.into_deserializer()).map(Some),
      None => Ok(None),
    }
  }

  fn next_value_seed<V: DeserializeSeed<'de>>(&mut self, seed: V) -> Result<V::Value> {
    seed.deserialize(Deserializer::begin(self.0)?)
  }
}

struct UnitEnumAccess<'ctx, R> {
  reader: &'ctx mut MtbonReader<R>,
  tag: StringTag,
}

impl<'de, 'ctx, R: Read> serde::de::EnumAccess<'de> for UnitEnumAccess<'ctx, R> {
  type Error = Error;
  type Variant = UnitVariantAccess;

  fn variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> Result<(V::Value, Self::Variant)> {
    let name = self.reader.parse_latin1_string(self.tag)?;
    let v = seed.deserialize(IntoDeserializer::<Self::Error>::into_deserializer(name))?;
    Ok((v, UnitVariantAccess))
  }
}

struct UnitVariantAccess;

impl<'de> serde::de::VariantAccess<'de> for UnitVariantAccess {
  type Error = Error;

  fn unit_variant(self) -> Result<()> {
    Ok(())
  }

  fn newtype_variant_seed<T: DeserializeSeed<'de>>(self, _seed: T) -> Result<T::Value> {
    Err(Error::invalid_type(
      Unexpected::NewtypeVariant,
      &"unit variant",
    ))
  }

  fn tuple_variant<V: Visitor<'de>>(self, _len: usize, _vis: V) -> Result<V::Value> {
    Err(Error::invalid_type(
      Unexpected::TupleVariant,
      &"unit variant",
    ))
  }

  fn struct_variant<V: Visitor<'de>>(
    self,
    _fields: &'static [&'static str],
    _vis: V,
  ) -> Result<V::Value> {
    Err(Error::invalid_type(
      Unexpected::StructVariant,
      &"unit variant",
    ))
  }
}

struct EnumAccess<'ctx, R>(&'ctx mut MtbonReader<R>);

impl<'de, 'ctx, R: Read> serde::de::EnumAccess<'de> for EnumAccess<'ctx, R> {
  type Error = Error;
  type Variant = VariantAccess<'ctx, R>;

  fn variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> Result<(V::Value, Self::Variant)> {
    match self.0.parse_object_key()? {
      Some(key) => {
        let name = IntoDeserializer::<Self::Error>::into_deserializer(key);
        let v = seed.deserialize(name)?;
        let access = VariantAccess(Deserializer::begin(self.0)?);
        Ok((v, access))
      }
      None => Err(Error::invalid_type(Unexpected::Map, &"enum variant")),
    }
  }
}

struct VariantAccess<'ctx, R>(Deserializer<'ctx, R>);

impl<'de, 'ctx, R: Read> serde::de::VariantAccess<'de> for VariantAccess<'ctx, R> {
  type Error = Error;

  fn unit_variant(self) -> Result<()> {
    serde::de::Deserialize::deserialize(self.0)
  }

  fn newtype_variant_seed<T: DeserializeSeed<'de>>(self, seed: T) -> Result<T::Value> {
    seed.deserialize(self.0)
  }

  fn tuple_variant<V: Visitor<'de>>(self, len: usize, vis: V) -> Result<V::Value> {
    serde::de::Deserializer::deserialize_tuple(self.0, len, vis)
  }

  fn struct_variant<V: Visitor<'de>>(
    self,
    fields: &'static [&'static str],
    vis: V,
  ) -> Result<V::Value> {
    serde::de::Deserializer::deserialize_struct(self.0, "", fields, vis)
  }
}
