use std::fmt::Display;

#[derive(Debug, thiserror::Error)]
#[error(transparent)]
pub struct Error {
  // We box the error data to reduce the size on stack.
  err: Box<ErrorKind>,
}

impl From<ErrorKind> for Error {
  fn from(kind: ErrorKind) -> Self {
    Error {
      err: Box::new(kind),
    }
  }
}

#[derive(Debug, thiserror::Error)]
pub enum ErrorKind {
  #[error("{}", .0)]
  Custom(Box<str>),
  #[error(transparent)]
  Io(std::io::Error),
  #[error("invalid MT64 byte found: {:x}", .0)]
  InvalidMt64(u8),
  #[error("unrepresentable non-latin1 char in string: {:?}", .0)]
  UnrepresentableChar(char),
  #[error("unrepresentable integer: {}", .0)]
  UnrepresentableInt(Box<str>),
  #[error("string is too big: {}", .0)]
  StringTooBig(usize),
  #[error("invalid negative length: {}", .0)]
  InvalidNegativeLen(i32),
  #[error("invalid float")]
  InvalidFloat,
  #[error("too many consecutive nulls in array")]
  TooManyNulls,
  #[error("key must be a string")]
  UnsupportedKeyType,
  #[error("unknown key id: {}", .0)]
  UnknownKeyId(u32),
}

impl serde::ser::Error for Error {
  fn custom<T: Display>(msg: T) -> Self {
    let kind = ErrorKind::Custom(msg.to_string().into());
    kind.into()
  }
}

impl serde::de::Error for Error {
  fn custom<T: Display>(msg: T) -> Self {
    let kind = ErrorKind::Custom(msg.to_string().into());
    kind.into()
  }
}
