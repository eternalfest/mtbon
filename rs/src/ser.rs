use std::convert::TryFrom;
use std::io::Write;

use serde::ser::Impossible;

use crate::error::{Error, ErrorKind};
use crate::writer::{ArrayFlavor, MtbonFlavor, MtbonWriter, ObjFlavor};

type Result<T> = std::result::Result<T, Error>;

pub struct Serializer<'ctx, W, F>(pub MtbonWriter<'ctx, 'static, W, F>);

macro_rules! try_serialize_int {
  ($ser_fn:ident($ty:ident) => $emit_fn:ident($emit_ty:ident $(; min: $min_value:literal)?)) => {
    #[inline]
    fn $ser_fn(self, v: $ty) -> Result<Self::Ok> {
      const MAX_SAFE_INT: $ty = 1 << 52;
      // Emit too large integers as floats, if the number is exactly representable.
      match $emit_ty::try_from(v) {
        Ok(v) => self.0.$emit_fn(v),
        Err(_) => {
          let min = try_serialize_int!(@min $($min_value)?);
          if (min..=MAX_SAFE_INT).contains(&v) {
            self.0.emit_f64(v as f64)
          } else {
            Err(ErrorKind::UnrepresentableInt(v.to_string().into()).into())
          }
        }
      }
    }
  };

  (@min) => { -MAX_SAFE_INT };
  (@min $val:literal) => { $val };
}

impl<'a, 'ctx, W: Write, F: MtbonFlavor> serde::Serializer for &'a mut Serializer<'ctx, W, F> {
  type Ok = ();
  type Error = Error;

  type SerializeSeq = Serializer<'a, W, ArrayFlavor<0>>;
  type SerializeTuple = Serializer<'a, W, ArrayFlavor<0>>;
  type SerializeTupleStruct = Serializer<'a, W, ArrayFlavor<0>>;
  type SerializeTupleVariant = Serializer<'a, W, ArrayFlavor<1>>;
  type SerializeMap = Serializer<'a, W, ObjFlavor<0>>;
  type SerializeStruct = Serializer<'a, W, ObjFlavor<0>>;
  type SerializeStructVariant = Serializer<'a, W, ObjFlavor<1>>;

  #[inline]
  fn is_human_readable(&self) -> bool {
    false
  }

  #[inline]
  fn serialize_bool(self, v: bool) -> Result<Self::Ok> {
    self.0.emit_bool(v)
  }

  #[inline]
  fn serialize_i8(self, v: i8) -> Result<Self::Ok> {
    self.0.emit_i16(v.into())
  }

  #[inline]
  fn serialize_i16(self, v: i16) -> Result<Self::Ok> {
    self.0.emit_i16(v)
  }

  #[inline]
  fn serialize_i32(self, v: i32) -> Result<Self::Ok> {
    self.0.emit_i32(v)
  }

  #[inline]
  fn serialize_u8(self, v: u8) -> Result<Self::Ok> {
    self.0.emit_u16(v.into())
  }

  #[inline]
  fn serialize_u16(self, v: u16) -> Result<Self::Ok> {
    self.0.emit_u16(v)
  }

  #[inline]
  fn serialize_u32(self, v: u32) -> Result<Self::Ok> {
    // Emit large u32s as floats (this doesn't loose any precision)
    match i32::try_from(v) {
      Ok(v) => self.0.emit_i32(v),
      Err(_) => self.0.emit_f64(v.into()),
    }
  }

  try_serialize_int!(serialize_i64(i64) => emit_i32(i32));
  try_serialize_int!(serialize_i128(i128) => emit_i32(i32));
  try_serialize_int!(serialize_u64(u64) => emit_i32(i32; min: 0));
  try_serialize_int!(serialize_u128(u128) => emit_i32(i32; min: 0));

  #[inline]
  fn serialize_f32(self, v: f32) -> Result<Self::Ok> {
    self.0.emit_f64(v.into())
  }

  #[inline]
  fn serialize_f64(self, v: f64) -> Result<Self::Ok> {
    self.0.emit_f64(v)
  }

  #[inline]
  fn serialize_char(self, v: char) -> Result<Self::Ok> {
    let mut buf = [0; 4];
    self.0.emit_str(v.encode_utf8(&mut buf))
  }

  #[inline]
  fn serialize_str(self, v: &str) -> Result<Self::Ok> {
    self.0.emit_str(v)
  }

  #[inline]
  fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok> {
    let mut seq = self.0.begin_array([])?;
    for b in v {
      seq.emit_u16((*b).into())?;
    }
    seq.end_compound()
  }

  #[inline]
  fn serialize_none(self) -> Result<Self::Ok> {
    self.0.emit_null()
  }

  #[inline]
  fn serialize_some<T: ?Sized>(self, value: &T) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    value.serialize(self)
  }

  #[inline]
  fn serialize_unit(self) -> Result<Self::Ok> {
    self.0.emit_null()
  }

  #[inline]
  fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok> {
    self.0.emit_null()
  }

  #[inline]
  fn serialize_unit_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    variant: &'static str,
  ) -> Result<Self::Ok> {
    self.0.emit_str(variant)
  }

  #[inline]
  fn serialize_newtype_struct<T: ?Sized>(self, _name: &'static str, value: &T) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    value.serialize(self)
  }

  #[inline]
  fn serialize_newtype_variant<T: ?Sized>(
    self,
    _name: &'static str,
    _variant_index: u32,
    variant: &'static str,
    value: &T,
  ) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    let mut obj = Serializer(self.0.begin_object([])?);
    obj.0.emit_borrowed_key(variant)?;
    value.serialize(&mut obj)?;
    obj.0.end_compound()
  }

  #[inline]
  fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
    self.0.begin_array([]).map(Serializer)
  }

  #[inline]
  fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
    self.0.begin_array([]).map(Serializer)
  }

  #[inline]
  fn serialize_tuple_struct(
    self,
    _name: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeTupleStruct> {
    self.0.begin_array([]).map(Serializer)
  }

  #[inline]
  fn serialize_tuple_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    variant: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeTupleVariant> {
    self.0.begin_array([variant]).map(Serializer)
  }

  #[inline]
  fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
    self.0.begin_object([]).map(Serializer)
  }

  #[inline]
  fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
    self.0.begin_object([]).map(Serializer)
  }

  #[inline]
  fn serialize_struct_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    variant: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeStructVariant> {
    self.0.begin_object([variant]).map(Serializer)
  }
}

macro_rules! impl_serialize_compound {
  (
    trait: $trait:path;
    fn: $value_fn:ident $(with_key($value_fn_key:ident))?;
    $(extra: { $($extra:tt)* })?
  ) => {
    impl<'ctx, W: Write, F: MtbonFlavor> $trait for Serializer<'ctx, W, F> {
      type Ok = ();
      type Error = Error;

      #[inline]
      fn $value_fn<T: ?Sized>(&mut self, $($value_fn_key: &'static str,)? value: &T) -> Result<()>
      where
          T: serde::Serialize {
        $(
          self.0.emit_borrowed_key($value_fn_key)?;
        )?
        value.serialize(self)
      }

      #[inline]
      fn end(mut self) -> Result<Self::Ok> {
        self.0.end_compound()
      }

      $($($extra)*)?
    }
  };
}

impl_serialize_compound! {
  trait: serde::ser::SerializeSeq;
  fn: serialize_element;
}

impl_serialize_compound! {
  trait: serde::ser::SerializeTuple;
  fn: serialize_element;
}

impl_serialize_compound! {
  trait: serde::ser::SerializeTupleStruct;
  fn: serialize_field;
}

impl_serialize_compound! {
  trait: serde::ser::SerializeTupleVariant;
  fn: serialize_field;
}

impl_serialize_compound! {
  trait: serde::ser::SerializeMap;
  fn: serialize_value;
  extra: {
    #[inline]
    fn serialize_key<T: ?Sized>(&mut self, key: &T) -> Result<()>
    where
        T: serde::Serialize {
      key.serialize(MapKeySerializer(&mut self.0))
    }
  }
}

impl_serialize_compound! {
  trait: serde::ser::SerializeStruct;
  fn: serialize_field with_key(key);
}

impl_serialize_compound! {
  trait: serde::ser::SerializeStructVariant;
  fn: serialize_field with_key(key);
}

struct MapKeySerializer<'a, 'ctx, W, F>(&'a mut MtbonWriter<'ctx, 'static, W, F>);

macro_rules! serialize_unsupported_key {
  ($fn_name:ident($ty:ty)) => {
    #[inline]
    fn $fn_name(self, _v: $ty) -> Result<Self::Ok> {
      unsupported_key_type()
    }
  };
}

impl<'a, 'ctx, W: Write, F: MtbonFlavor> serde::Serializer for MapKeySerializer<'a, 'ctx, W, F> {
  type Ok = ();
  type Error = Error;

  type SerializeSeq = Impossible<Self::Ok, Self::Error>;
  type SerializeTuple = Impossible<Self::Ok, Self::Error>;
  type SerializeTupleStruct = Impossible<Self::Ok, Self::Error>;
  type SerializeTupleVariant = Impossible<Self::Ok, Self::Error>;
  type SerializeMap = Impossible<Self::Ok, Self::Error>;
  type SerializeStruct = Impossible<Self::Ok, Self::Error>;
  type SerializeStructVariant = Impossible<Self::Ok, Self::Error>;

  #[inline]
  fn serialize_char(self, v: char) -> Result<Self::Ok> {
    let mut buf = [0; 4];
    self.0.emit_key(v.encode_utf8(&mut buf))
  }

  #[inline]
  fn serialize_str(self, v: &str) -> Result<Self::Ok> {
    self.0.emit_key(v)
  }

  serialize_unsupported_key!(serialize_i8(i8));
  serialize_unsupported_key!(serialize_i16(i16));
  serialize_unsupported_key!(serialize_i32(i32));
  serialize_unsupported_key!(serialize_i64(i64));
  serialize_unsupported_key!(serialize_i128(i128));
  serialize_unsupported_key!(serialize_u8(u8));
  serialize_unsupported_key!(serialize_u16(u16));
  serialize_unsupported_key!(serialize_u32(u32));
  serialize_unsupported_key!(serialize_u64(u64));
  serialize_unsupported_key!(serialize_u128(u128));
  serialize_unsupported_key!(serialize_bool(bool));
  serialize_unsupported_key!(serialize_f32(f32));
  serialize_unsupported_key!(serialize_f64(f64));
  serialize_unsupported_key!(serialize_bytes(&[u8]));

  #[inline]
  fn serialize_none(self) -> Result<Self::Ok> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_some<T: ?Sized>(self, _value: &T) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_unit(self) -> Result<Self::Ok> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_unit_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    _variant: &'static str,
  ) -> Result<Self::Ok> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_newtype_struct<T: ?Sized>(self, _name: &'static str, _value: &T) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_newtype_variant<T: ?Sized>(
    self,
    _name: &'static str,
    _variant_index: u32,
    _variant: &'static str,
    _value: &T,
  ) -> Result<Self::Ok>
  where
    T: serde::Serialize,
  {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_tuple_struct(
    self,
    _name: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeTupleStruct> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_tuple_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    _variant: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeTupleVariant> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
    unsupported_key_type()
  }

  #[inline]
  fn serialize_struct_variant(
    self,
    _name: &'static str,
    _variant_index: u32,
    _variant: &'static str,
    _len: usize,
  ) -> Result<Self::SerializeStructVariant> {
    unsupported_key_type()
  }
}

fn unsupported_key_type<T>() -> Result<T> {
  Err(ErrorKind::UnsupportedKeyType.into())
}
